var server = require('http').createServer();
var url = require('url');
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({server: server});
var express = require('express');
var uuid = require('node-uuid');

var Auth = require('./js/Userauth').Userauth;
var Game = require('./js/Game').Game;

//port config
//TODO: set this to whatever it needs to be
var port = 5000;




//rest api
var app = express();
app.use(function(req, res) {
	res.send({msg: 'test endpoint please ignore'});
});


//game user auth
var userauth = new Auth();

//game logic
var game = new Game(userauth);

//web socket server
wss.on('connection', function connection(ws) {
	var location = url.parse(ws.upgradeReq.url, true);
	
	userauth.addConnection(ws);
});

wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
		client.send(data);
	});
};





server.on('request', app);
server.listen(port, function() {console.log('listening on ' + server.address().port)});