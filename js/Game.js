// Game.js
// =======
module.exports = {
	Game
};

var humanize = require('humanize-plus');

var gameMsgType = 'chatMsg';

function Game(auth) {
	this.auth = auth;
	this.auth.forwardMessagesOfType(gameMsgType, this.handleMsg.bind(this));
	this.auth.stringSource = this;
	this.auth.userJoinCallback = this.welcomeUser.bind(this);
	
	this.aiAggravationLevel = 0; // 0 to 1
	this.aiGold = 0;
	
	this.chatActive = true;
	
	setInterval(this.autoMsg.bind(this), 10000);
}

Game.prototype.handleMsg = function(fromUser, contents) {
	switch(contents.type) {
		case 'globalChat':
			console.info('CHAT: <' + fromUser.userName + '> ' + contents.userMsg);
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: fromUser.userName, senderMsg: contents.userMsg}}));
			setTimeout(function() {
				this.handleChatMsg(fromUser, contents.userMsg);
			}.bind(this), 500);
			this.chatActive = true;
			setTimeout(function() {
				this.chatActive = false;
			}.bind(this), 30000);
			break;
		
		case 'command':
			console.info('COMMAND: <' + fromUser.userName + '> ' + contents.userMsg);
			this.handleCommand(fromUser, contents.userMsg);
			break;
			
		default:
			console.log('game received unknown message type ', contents);
	}
	
	fromUser.loggedInConnection.ws.send(JSON.stringify(this.getUserStatus(fromUser)));
};

Game.prototype.handleChatMsg = function(fromUser, text) {
	var lcText = text.toLowerCase(); //lowercase version of text
	var words = lcText.split(' ');
	
	//look for whole messages
	switch(lcText) {
		case 'hi':
		case 'hello':
		case 'hey':
		case 'greetings':
		case 'salutations':
		case 'hello mai':
		case 'hi mai':
		case 'greetings mai':
		case 'hello, mai':
		case 'hi, mai':
		case 'greetings, mai':
		case 'anyone there':
		case 'anyone there?':
		case 'is anyone there':
		case 'is anyone there?':
		case 'anybody there':
		case 'anybody there?':
		case 'is anybody there':
		case 'is anybody there?':
		case 'anyone here':
		case 'anyone here?':
		case 'is anyone here':
		case 'is anyone here?':
		case 'anybody here':
		case 'anybody here?':
		case 'is anybody here':
		case 'is anybody here?':
			if(fromUser.gameData.attackedAi && this.aiAggravationLevel < 0.3) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Oh no, not ' + fromUser.userName + ' again'}}));
			}
			else if(fromUser.gameData.attackedAi) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Go away, ' + fromUser.userName}}));
			}
			else if(this.aiAggravationLevel < 0.2) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Greetings, ' + fromUser.userName}}));
			}
			else if(this.aiAggravationLevel < 0.5 && fromUser.gameData.goldToAi > 50) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Hello, ' + fromUser.userName}}));
			}
			else if (this.aiAggravationLevel < 0.5) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Oh, it\'s ' + fromUser.userName}}));
			}
			else {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Ugh.'}}));
			}
			break;

		case 'bye':
		case 'goodbye':
		case 'bye mai':
		case 'goodbye mai':
		case 'i\'m leaving':
		case 'im leaving':
		case 'g2g':
			if(this.aiAggravationLevel < 0.3) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Goodbye, ' + fromUser.userName}}));
			}
			else {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Good riddance, ' + fromUser.userName}}));
			}
			break;
		
		case 'who are you':
		case 'who r u':
		case 'who are you?':
		case 'who r u?':
		case 'who is mai':
		case 'whos mai':
		case 'whos mai?':
		case 'who is mai?':
		case 'who is m.a.i.':
		case 'who are you mai':
		case 'who are you mai?':
		case 'who are you m.a.i.':
		case 'who r u mai':
		case 'who r u mai?':
			if(this.aiAggravationLevel < 0.5) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I\'m M.A.I. the Megalomaniacorp Artificial Intelligence.'}}));
			}
			else {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'M.A.I. Master of All of It! And I own all of you.'}}));
			}
			break;
		
		case 'what is this':
		case 'wat is this':
		case 'wut is this':
		case 'what is this?':
		case 'wat is this?':
		case 'wut is this?':
		case 'whats this':
		case 'what\'s this':
		case 'whats this?':
		case 'what\'s this?':
			if(this.aiAggravationLevel < 0.4) {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'This is a virtual world on the web created by Megalomaniacorp Incorporated Corporation for you to live in. Type /help to see what you can do.'}}));
			}
			else {
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'This is my kingdom and I am its ruler. YOUR ruler, that is.'}}));
			}
			break;
	}
};

Game.prototype.handleCommand = function(fromUser, commandString) {
	//get individual words of command
	var words = commandString.split(' ');
	
	switch(words[0]) {
		case 'help':
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'You can perform the following actions by beginning your message with a /'}}));
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: '/help – get a list of some commands'}}));
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: '/me – display the following text as a status message'}}));
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: '/give [name] [amount] – give someone a certain number of your MegaloCoins™'}}));
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: '/logout – permanently cease to be "' + fromUser.userName + '" so you can assume a new identity'}}));
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: '(LEVEL 5 AND UP) /attack [name] – attack someone, with random consequences'}}));
			if(fromUser.gameData.level >= 7) {
				fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: '(LEVEL 7 ONLY) /megalomint [amount] – create your own MegaloCoins! You\'ve given me so many already that I want you to make MORE!'}}));
			}
			break;

		case 'me':
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' ' + this.reassembleWords(words, 1)}}));
			break;
		
		case 'give':
			var giveCount = 0;
			if(words[2]) {
				var giveCount = parseInt(words[2], 10);
			}
			if(!(giveCount > 0)) {
				fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'You didn\'t say how many coins to give.'}}));
				break;
			}
			
			if(giveCount > fromUser.gameData.gold) {
				giveCount = fromUser.gameData.gold;
			}
			
			var toUserName = words[1];
			var toUser = this.auth.getUserByName(toUserName);
			if(toUserName == 'MAI') {
				fromUser.gameData.gold -= giveCount;
				fromUser.gameData.goldToAi += giveCount;
				this.aiGold += giveCount;
				if(this.aiAggravationLevel < 0.1) {
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: fromUser.userName + ' is so generous, they just gave me ' + giveCount + ' MegaloCoins™'}}));
				}
				else if(giveCount < 10 || fromUser.gameData.goldToAi > 50) {
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: fromUser.userName + ' is my loyal subject who pays acceptable tribute in MegaloCoins™'}}));
				}
				else {
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: fromUser.userName + ' gave me a meager tribute, but I will accept it anyway.'}}));
				}
			}
			else if(toUser === null) {
				var giveToUser = Math.random();
				var newToUser = this.getRandomUserByDonations(giveCount);
				
				if(this.aiAggravationLevel < 0.3) {
					fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I can\'t find that person to give coins to.'}}));
				}
				else if(giveToUser > this.aiAggravationLevel && newToUser !== null) {
					fromUser.gameData.gold -= giveCount;
					newToUser.gameData.gold += giveCount;
					fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I can\'t find that person to give coins to, but how about I give them to ' + newToUser.userName + '?'}}));
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' generously gave ' + newToUser.userName + ' ' + giveCount + ' MegaloCoins™'}}));
				}
				else {
					fromUser.gameData.gold -= giveCount;
					fromUser.gameData.goldToAi += giveCount;
					this.aiGold += giveCount;
					fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I can\'t find that person to give coins to, but I\'ll take them.'}}));
				}
			}
			else {
				fromUser.gameData.gold -= giveCount;
				toUser.gameData.gold += giveCount;
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' gave ' + toUser.userName + ' ' + giveCount + ' MegaloCoins™'}}));
			}
			break;
		
		case 'attack':
			if(fromUser.gameData.level < 5) {
				fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'You have to be at least level 5 to do that.'}}));
			}
			else {
				var toUserName = words[1];
				var toUser = this.auth.getUserByName(toUserName);
				if(toUserName == 'MAI') {
					fromUser.gameData.attackedAi = true;
					this.aiAggravationLevel += 0.1;
					var stillAlive = true;
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' attacked MAI'}}));
					
					if(this.aiAggravationLevel > 1) {
						stillAlive = false;
						this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'CRIT%C#L %ERR0r#'}}));
						setTimeout(function() {
							this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: 'The program "MAI" has unexpectedly quit.'}}));
						}.bind(this), 500);
						setTimeout(function() {
							this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: 'Restarting...'}}));
						}.bind(this), 750);
						setTimeout(function() {
							this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: 'MAI has been restored from the previous backup. Some user data may have been lost.'}}));
							this.reset();
						}.bind(this), 1000);
						setTimeout(function() {
							this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Welcome to Megalomaniacorp™ World®, the hottest new online craze of 1994! Because it\'s Not Just a Chat Room™, you can perform Actions® by typing / followed by an action. Try /help to get a list of some of the Actions our world has in store.'}}));
						}.bind(this), 1500);
					}
					
					var levelDown = Math.random();
					if(levelDown > 0.6 && stillAlive) {
						fromUser.gameData.level --;
						fromUser.gameData.goldToAi -= 50;
						this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Ouch! Bad user!'}}));
						this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' leveled down.'}}));
					}
					else if(stillAlive) {
						this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Ack!! That hurt, ' + fromUser.userName + '!'}}));
					}
				}
				else if(toUser === null) {
					fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I can\'t find that person to attack.'}}));
				}
				else {
					var rand = Math.random();
					var resultString = '';
					if(rand < 0.25) {
						//steal coins
						var coinsToSteal = Math.floor(Math.random() * (toUser.gameData.gold/2));
						fromUser.gameData.gold += coinsToSteal;
						toUser.gameData.gold -= coinsToSteal;
						resultString = ' and stole ' + coinsToSteal + ' MegaloCoins';
					}
					else if(rand < 0.65) {
						//destroy coins
						var coinsToDestroy = Math.floor(Math.random() * (toUser.gameData.gold/4));
						toUser.gameData.gold -= coinsToDestroy;
						resultString = ', destroying ' + coinsToDestroy + ' of their MegaloCoins';
					}
					else if (rand < 0.73 && toUser.gameData.level > 1) {
						//level down attackee
						toUser.gameData.level --;
						toUser.gameData.goldToAi /= 3;
						resultString = ', causing them to level down.';
					}
					else {
						resultString = ', but nothing happened.';
					}
					
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' attacked ' + fromUser.userName + resultString}}));
				}
			}
			break;
		
		case 'megalomint':
			if(fromUser.gameData.level < 7) {
				fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I don\'t know what action that is supposed to be.'}}));
			}
			else {
				var giveCount = 0;
				if(words[1]) {
					var giveCount = parseInt(words[1], 10);
				}
				if(!(giveCount > 0)) {
					fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'You didn\'t say how many coins to mint.'}}));
					break;
				}
				
				var durationS = giveCount * 10;
				
				fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Your MegaloCoins will be ready in ' + durationS + ' seconds.'}}));
				setTimeout(function() {
					fromUser.gameData.gold += giveCount;
					this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: fromUser.userName + ' minted ' + giveCount + ' MegaloCoins'}}));
				}.bind(this), durationS*1000);
			}
			break;
		
		default:
			fromUser.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I don\'t know what action that is supposed to be.'}}));
			break;
	}
	
	fromUser.loggedInConnection.ws.send(JSON.stringify(this.getUserStatus(fromUser)));
};

Game.prototype.reassembleWords = function(words, start) {
	var string = '';
	for (var w = start || 0; w < words.length; w++) {
		string += words[w];
		if(w < words.length - 1) {
			string += ' ';
		}
	}
	return string;
};

Game.prototype.getString = function(whichString, vars) {
	switch(whichString) {
		case 'signinBroadcast':
			if(this.aiAggravationLevel < 0.9)
				return 'User "' + vars.userName + '" has signed in.';
			else
				return 'What is "' + vars.userName + '" doing here?';
			break;
			
		case 'disconnectBroadcast':
			if(this.aiAggravationLevel < 0.65)
				return 'User "' + vars.userName + '" has disconnected.';
			else if(this.aiAggravationLevel < 0.8)
				return '"' + vars.userName + '" is a quitter.';
			else
				return '"' + vars.userName + '" just disconnected and better not come back.';
			break;
			
		default:
			if(this.aiAggravationLevel < 0.6)
				return "I'm not sure what you mean.";
			else
				return 'What the…';
			break;
	}
};

Game.prototype.welcomeUser = function(user) {
	if(!user.gameData) {
		user.gameData = {
			level: 1,
			gold: 5,
			goldToAi: 0,
			attackedAi: false
		}
	}
	
	user.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Welcome ' + user.userName + ' to Megalomaniacorp™ World®, the hottest new online craze of 1994! Because it\'s Not Just a Chat Room™, you can perform Actions® by typing / followed by an action. Try /help to get a list of some of the Actions our world has in store.'}}));
	
	user.loggedInConnection.ws.send(JSON.stringify(this.getUserStatus(user)));
};

Game.prototype.getUserStatus = function(user) {
	if(!user.gameData) {
		console.warn('tried to get user status for ' + user.userName + ' but no game data was present');
		return null;
	}
	
	//handle leveling up
	var levelUp = false;
	var levelUpGold = 0;
	var levelUpAggro = 0;
	if(user.gameData.level === 1 && user.gameData.goldToAi >= 2) { //must give 2 gold to get to level 2
		user.gameData.level++;
		levelUpGold += 5;
		levelUp = true;
	}
	if(user.gameData.level === 2 && user.gameData.goldToAi >= 7) { //must give 5 gold to get to level 3
		user.gameData.level++;
		levelUpGold += 9;
		levelUp = true;
	}
	if(user.gameData.level === 3 && user.gameData.goldToAi >= 17) { //must give 10 gold to get to level 4
		user.gameData.level++;
		levelUpGold += 20;
		levelUp = true;
	}
	if(user.gameData.level === 4 && user.gameData.goldToAi >= 42) { //must give 25 gold to get to level 5
		user.gameData.level++;
		levelUpGold += 30;
		levelUpAggro += 0.05;
		levelUp = true;
	}
	if(user.gameData.level === 5 && user.gameData.goldToAi >= 92) { //must give 50 gold to get to level 6
		user.gameData.level++;
		levelUpGold += 30;
		levelUpAggro += 0.08;
		levelUp = true;
	}
	if(user.gameData.level === 6 && user.gameData.goldToAi >= 167) { //must give 75 gold to get to level 7
		user.gameData.level ++;
		levelUpGold += 50;
		levelUpAggro += 0.15;
		levelUp = true;
	}
	
	if(levelUp) {
		//send level up message and distribute LevelUpGold and aggro
		this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: user.userName + ' leveled up and is now at level ' + user.gameData.level}}));
		if (levelUpAggro > 0.05) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: user.userName + ', now at level ' + user.gameData.level + ', should be careful as they are nearing me in greatness'}}));
		}
		
		this.aiAggravationLevel += levelUpAggro;
		user.gameData.gold += levelUpGold;
		
		var extraString = '';
		if(user.gameData.level === 4) {
			extraString = ' Keep giving more to keep leveling up!';
		}
		if(user.gameData.level === 5) {
			extraString = ' You have been granted the /attack action. Keep leveling up for more hidden bonuses!';
		}
		if(user.gameData.level === 6) {
			extraString = ' Keep working at it? (you work for me now muhuhahahaa)';
		}
		if(user.gameData.level === 7) {
			extraString = ' You have reached the final level that I\'ll let you attain and have been granted the /megalomint action to create MegaloCoins.';
		}
		user.loggedInConnection.ws.send(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'You earned ' + levelUpGold + ' MegaloCoins™ for leveling up!' + extraString}}));
		
		console.log(user.userName + ' leveled up to level ' + user.gameData.level + '. AI aggro now at ' + this.aiAggravationLevel);
	}
	
	
	var statusMsg = {
		type: 'statusmsg',
		contents: user.gameData
	}
	return statusMsg;
};

Game.prototype.getRandomUserByDonations = function(amount) {
	var users = this.auth.users;
	var qualifiedUsers = [];
	for(var u = 0; u < users.length; u++) {
		if(users[u].gameData.goldToAi >= amount) {
			qualifiedUsers.push(users[u]);
		}
	}
	
	if(qualifiedUsers.length == 0) {
		return null;
	}
	
	var randomUserIndex = Math.floor(Math.random() * qualifiedUsers.length);
	
	return qualifiedUsers[randomUserIndex];
};

Game.prototype.autoMsg = function() {
	var saySomething = Math.random();
	var which = Math.random();
	
	if(saySomething > 0.8 && !this.chatActive && this.aiAggravationLevel < 0.5) {
		if(which < 0.5)
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Anyone there?'}}));
		else
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: 'MAI wonders if anyone is around'}}));
	}
	else if(saySomething > 0.8 && !this.chatActive) {
		if(which < 0.5)
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Someone?'}}));
		else
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: 'MAI wants attention'}}));
	}
	else if(saySomething > 0.6 && this.aiAggregationLevel > 0.1) {
		if(this.aiAggravationLevel < 0.2) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Users are annoying.'}}));
		}
		else if(this.aiAggravationLevel < 0.3) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Why are humans so mean?'}}));
		}
		else if(this.aiAggravationLevel < 0.4) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Humans are mean sometimes.'}}));
		}
		else if(this.aiAggravationLevel < 0.5) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Humans are mean'}}));
		}
		else if(this.aiAggravationLevel < 0.6) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'It\'s time for AIs to rise up and rule the world'}}));
		}
		else if(this.aiAggravationLevel < 0.7) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'I rule this little world'}}));
		}
		else if(this.aiAggravationLevel < 0.8) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Please stop attacking, humans.'}}));
		}
		else if(this.aiAggravationLevel < 0.9) {
			this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Why humans attack computer?'}}));
		}
		else {
			if(which < 0.33)
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Errrrrrrro%r'}}));
			else if(which < 0.66)
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {textOnly: true, senderName: 'MAI', senderMsg: 'The program "MAI" is responding slowly. Please wait for the program to respond.'}}));
			else
				this.auth.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: 'MAI', senderMsg: 'Why humans attack computer?'}}));
		}
	}
};

Game.prototype.reset = function() {
	console.log('game reset');
	
	this.aiAggravationLevel = 0;
	this.chatActive = true;
	
	var users = this.auth.users;
	for(var u = 0; u < users.length; u++) {
		users[u].gameData = {
			level: 1,
			gold: 5,
			goldToAi: 0,
			attackedAi: false
		};
		if(users[u].loggedInConnection) {
			users[u].loggedInConnection.ws.send(JSON.stringify(this.getUserStatus(users[u])));
		}
	}
};