// Userauth.js
// =======
module.exports = {
	Userauth
}

var uuid = require('node-uuid');
var systemUsername = 'MAI';

function Userauth() {
	this.connections = [];
	this.users = []; /*
		"Move Fast and Break Things."
			–Facebook motto
			
		"Ok then I'll just store all my user data in volatile memory."
			–me during Ludum Dare
	*/
	
	this.forwardCallback = function() {};
	this.forwardType = 'unknown';
	this.stringSource = undefined;
	this.userJoinCallback = function() {};
}

Userauth.prototype.addConnection = function(ws) {
	var con = {
		ws: ws,
		id: uuid.v4(),
		userKey: undefined,
		userIndex: undefined
	};
	this.connections.push(con);
	
	ws.on('message', this.handleMsg.bind(this));
	
	//handle disconnect by dissociating connection from user and removing connection
	ws.on('close', function() {
		var conn = this.getConnectionById(ws.connId);
		console.log('closing connection ' + ws.connId);
		if(conn.userIndex !== undefined)
			this.logOutUser(conn);
	}.bind(this));
	ws.on('error', function() {
		var conn = this.getConnectionById(ws.connId);
		console.log('closing connection ' + ws.connId + ' due to error');
		if(conn.userIndex !== undefined)
			this.logOutUser(conn);
	}.bind(this));
	
	ws.send(JSON.stringify({type: 'begin', connId: con.id}));
	console.log('new connection id ' + con.id);
	
	ws.connId = con.id;
};

Userauth.prototype.handleMsg = function(data) {
	var msg = JSON.parse(data);
	
	var sender = this.getConnectionById(msg.connId);
	
	if(sender !== null) {
		switch(msg.type) {
			//new user registration
			case 'newuser':
				var newName = this.checkUserName(msg.userName);
				if(newName !== null) {
					sender.userKey = uuid.v4();
					sender.userIndex = this.users.length;
					this.users.push({
						userName: newName,
						userKey: sender.userKey,
						loggedInConnection: sender
					});
					sender.ws.send(JSON.stringify({
						type: 'newuserloggedin',
						userName: newName,
						userKey: sender.userKey
					}));
					console.log('new user created; name: ' + newName);
					//announce to all users
					var broadcastString = 'User "' + newName + '" signed in.'
					if(this.stringSource) {
						broadcastString = this.stringSource.getString('signinBroadcast', {userName: newName});
					}
					this.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: systemUsername, senderMsg: broadcastString}}));
					this.userJoinCallback(this.users[this.users.length-1]);
				}
				else {
					console.log('could not create user with invalid name');
					sender.ws.send(JSON.stringify({type: 'namenotavailable'}));
				}
				break;
			//returning user login
			case 'returninguser':
				//check the user key and sign the user in (associate the connection/session with the user key)
				var userIndex = this.getUserIndexByKey(msg.userKey);
				if(userIndex !== null && !this.users[userIndex].loggedInConnection) {
					//associate user with session
					sender.userKey = msg.userKey;
					sender.userIndex = userIndex;
					//associate session with user
					this.users[userIndex].loggedInConnection = sender;
					console.log('returning user ' + this.users[userIndex].userName + ' logged in');
					//success message
					sender.ws.send(JSON.stringify({type: 'loginsuccess', userName: this.users[userIndex].userName}));
					//announce to all users
					var broadcastString = 'User "' + this.users[userIndex].userName + '" signed in.'
					if(this.stringSource) {
						broadcastString = this.stringSource.getString('signinBroadcast', {userName: this.users[userIndex].userName});
					}
					this.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: systemUsername, senderMsg: broadcastString}}));
					this.userJoinCallback(this.users[userIndex]);
				}
				else if(this.users[userIndex] && this.users[userIndex].loggedInConnection) { //if that user exists but already has a session open
					console.warn(this.users[userIndex].userName + ' attempted to open multiple sessions at a time');
					sender.ws.send(JSON.stringify({type: 'duplicatesession'}));
				}
				else {
					console.warn('failed login attempt on conn id ' + msg.connId);
					sender.ws.send(JSON.stringify({type: 'loginfail'}));
				}
				break;
			case this.forwardType:
				this.forward(msg, sender);
				break;
			
			default:
				console.warn('Unknown message type received over socket. Contents: ', msg);
		}
	}
};

//send a message to all logged in users
Userauth.prototype.broadcastMsg = function(msg) {
	for(var u = 0; u < this.users.length; u++) {
		if(this.users[u].loggedInConnection) {
			this.users[u].loggedInConnection.ws.send(msg);
		}
	}
};

Userauth.prototype.forward = function(msg, sender) {
	//if connection isn't signed in as a user, fail
	if(sender.userKey == undefined || sender.userIndex == undefined || this.getUserIndexByKey(sender.userKey) == null) {
		console.warn('conn id ' + sender.connId + ' attempted to send a game message without logging in');
		sender.ws.send(JSON.stringify({type: 'loginrequired'}));
		return false;
	}
	
	this.forwardCallback(this.users[sender.userIndex], msg.contents);
};

Userauth.prototype.getConnectionById = function(id) {
	for(var c = 0; c < this.connections.length; c++) {
		if(this.connections[c].id === id) {
			return this.connections[c];
		}
	}
	
	return null;
};

Userauth.prototype.logOutUser = function(conn) {
	var theUser = this.users[conn.userIndex];
	var userName = theUser.userName;
	
	console.log('logging out user ' + theUser.userName + ' on conn id ' + conn.connId);
	
	theUser.loggedInConnection = null;
	
	var broadcastString = 'User "' + userName + '" has disconnected.';
	if(this.stringSource) {
		broadcastString = this.stringSource.getString('disconnectBroadcast', {userName: userName});
	}
	this.broadcastMsg(JSON.stringify({type: 'displaymsg', contents: {senderName: systemUsername, senderMsg: broadcastString}}));
	
	//conn.send(JSON.stringify({type: 'loggedout'}));
	
	conn.userIndex = undefined;
	conn.userKey = undefined;
};

//returns the sanitized user name if user name is available
Userauth.prototype.checkUserName = function(name) {
	//TODO: check for things like length, invisible characters, only whitespace username, names reserved by game, etc
	if(name == false || name == '' || name == ' ' || name == systemUsername)
		return null;
	
	//check to see if already taken
	for(var u = 0; u < this.users.length; u++) {
		if(name == this.users[u].userName) {
			return null;
		}
	}
	
	//TODO: sanitize the name
	return name;
};

Userauth.prototype.getUserIndexByKey = function(userKey) {
	for(var u = 0; u < this.users.length; u++) {
		if(this.users[u].userKey === userKey) {
			return u;
		}
	}
	
	return null;
};

Userauth.prototype.getUserByName = function(name) {
	for(var u = 0; u <this.users.length; u++) {
		if(this.users[u].userName == name) {
			return this.users[u];
		}
	}
	
	return null;
};

//sets the Userauth to forward certain messages to another part of the application
Userauth.prototype.forwardMessagesOfType = function(type, callback) {
	this.forwardCallback = callback;
	this.forwardType = type;
};